import sys
sys.path.append("modules")

from modules import requests, feedparser


def simple_get(url):
    """
    Attempts to get the content at `url` by making an HTTP GET request.
    If the content-type of the response is HTML, return the text content; otherwise, return None.
    """
    try:
        response = requests.get(url)
        if response.status_code == 200:
            return response.text
        else:
            return None

    except requests.exceptions.RequestException as e:
        log_error('Error during requests to {0} : {1}'.format(url, str(e)))
        return None

def is_good_response(resp):
    """
    Returns True if the response seems to be HTML, False otherwise.
    """
    content_type = resp.headers['Content-Type'].lower()
    return (resp.status_code == 200
            and content_type is not None
            and content_type.find('html') > -1)

def log_error(e):
    """
    It is always a good idea to log errors.
    This function just prints them, but you can make it do anything.
    """
    print(e)

def get_debian_news():
    rss_feed_url = "https://www.debian.org/News/news"

    # Fetch the RSS feed content using simple_get
    rss_content = simple_get(rss_feed_url)

    if rss_content:
        # Parse the RSS feed
        feed = feedparser.parse(rss_content)

        if 'entries' in feed:
            entries = feed.entries

            # Create a Markdown file to write the content
            with open('debian_news.md', 'w', encoding='utf-8') as md_file:
                # Write the title of the RSS feed
                md_file.write("# Debian News\n\n")

                for entry in entries:
                    # Write the title and publication date of each entry as a Markdown heading
                    md_file.write(f"## {entry.title}\n")

                    # Check if 'published_parsed' attribute exists before using it
                    if 'published_parsed' in entry:
                        published_date = entry.published_parsed
                        published_str = f"Published on {published_date.tm_year}-{published_date.tm_mon:02d}-{published_date.tm_mday:02d}"
                        md_file.write(f"{published_str}\n\n")

                    # Write the content of the entry as a Markdown paragraph
                    md_file.write(f"{entry.summary}\n\n")

            print("Debian News content has been saved to debian_news.md")
        else:
            print("Failed to retrieve content from the Debian News RSS feed.")
    else:
        print("Failed to retrieve RSS feed content.")

if __name__ == "__main__":
    get_debian_news()
